r-cran-bold (1.3.0-1) unstable; urgency=medium

  * New upstream version
  * dh-update-R to update Build-Depends (routine-update)
  * No need to remove anything from upstream tarball

 -- Andreas Tille <tille@debian.org>  Sun, 11 Jun 2023 18:46:52 +0200

r-cran-bold (1.2.0-2) unstable; urgency=medium

  * Disable reprotest
  * Standards-Version: 4.6.2 (routine-update)
  * Make sure test data will be uncompressed (otherwise test times out)

 -- Andreas Tille <tille@debian.org>  Wed, 11 Jan 2023 15:00:35 +0100

r-cran-bold (1.2.0-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.0 (routine-update)
  * dh-update-R to update Build-Depends (3) (routine-update)

 -- Andreas Tille <tille@debian.org>  Thu, 09 Sep 2021 12:28:00 +0200

r-cran-bold (1.1.0+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * debhelper-compat 13 (routine-update)

 -- Dylan Aïssi <daissi@debian.org>  Wed, 29 Jul 2020 09:41:03 +0200

r-cran-bold (1.0.0+dfsg-2) unstable; urgency=medium

  * Set upstream metadata fields: Archive, Bug-Database, Bug-Submit,
    Repository, Repository-Browse.

 -- Andreas Tille <tille@debian.org>  Mon, 18 May 2020 22:15:20 +0200

r-cran-bold (1.0.0+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.5.0 (routine-update)
  * debhelper-compat 12 (routine-update)
  * Testsuite: autopkgtest-pkg-r (routine-update)
  * autopkgtest: s/ADTTMP/AUTOPKGTEST_TMP/g (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)

 -- Dylan Aïssi <daissi@debian.org>  Wed, 06 May 2020 10:18:20 +0200

r-cran-bold (0.9.0+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * debhelper 12
  * Standards-Version: 4.4.0

 -- Dylan Aïssi <daissi@debian.org>  Fri, 19 Jul 2019 15:28:10 +0200

r-cran-bold (0.8.6+dfsg-2) unstable; urgency=medium

  * Test-Depends: r-cran-vcr
    Closes: #916723

 -- Andreas Tille <tille@debian.org>  Mon, 17 Dec 2018 22:10:12 +0100

r-cran-bold (0.8.6+dfsg-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Mon, 17 Dec 2018 11:18:00 +0100

r-cran-bold (0.8.0+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.2.1

 -- Andreas Tille <tille@debian.org>  Tue, 30 Oct 2018 10:46:59 +0100

r-cran-bold (0.5.0+dfsg-1) unstable; urgency=medium

  * Rebuild for R 3.5 transition
  * debhelper 11
  * Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-
    lists.debian.net>
  * Point Vcs fields to salsa.debian.org
  * dh-update-R to update Build-Depends
  * Secure URI in watch file
  * Remove autogenerated HTML docs since it is including compressed JS

 -- Andreas Tille <tille@debian.org>  Mon, 04 Jun 2018 21:39:06 +0200

r-cran-bold (0.5.0-1) unstable; urgency=medium

  * New upstream version
  * Moved packaging from SVN to Git
  * Standards-Version: 4.1.1
  * Add debian/README.source to document binary data files
  * Build-Depends: r-cran-crul

 -- Andreas Tille <tille@debian.org>  Sun, 01 Oct 2017 22:48:25 +0200

r-cran-bold (0.4.0-1) unstable; urgency=medium

  * New upstream version
  * debhelper 10
  * d/watch: version=4
  * New Build-Depends: r-cran-data.table, r-cran-tibble

 -- Andreas Tille <tille@debian.org>  Sun, 08 Jan 2017 08:30:33 +0100

r-cran-bold (0.3.5-1) unstable; urgency=medium

  * New upstream version
  * Convert to dh-r
  * Canonical homepage for CRAN
  * New Build-Depends: r-cran-xml2

 -- Andreas Tille <tille@debian.org>  Tue, 08 Nov 2016 11:38:13 +0100

r-cran-bold (0.3.0-1) unstable; urgency=low

  * Initial release (Closes: #819226)

 -- Andreas Tille <tille@debian.org>  Fri, 25 Mar 2016 07:37:04 +0100
